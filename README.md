# The nonlinear Schrödinger equation for orthonormal functions: I. Existence of ground states.

This is the code + pictures of the article:

_The nonlinear Schrödinger equation for orthonormal functions_: I. Existence of ground states.
David Gontier, Mathieu Lewin, Faizan Q. Nazar
Arch. Rational Mech. Anal. 240, 1203–1254 (2021).

journal version: https://link.springer.com/article/10.1007/s00205-021-01634-7

arxiv version: https://arxiv.org/abs/2002.04963

Author: David Gontier (2021)
