import numpy as np
import NLS_1d as NLS

##################################
# Fixed Parameters
##################################

L = 100 #computation on the segment [O, L]
N = 1 # number of electrons
p = 1.3 # the p value

Ng_list = range(500, 1000, 10)
ans = []
list_rho = []

###################################
for Ng in Ng_list:
    print("Computation for Ng = ", Ng)
    eps = L/Ng
    xx = np.linspace(0, L-eps, Ng) # the grid

    # constructing first guess
    rho0 = zeros(Ng)
    for i in range(N):
        rho0 += exp(-(xx+i*d-L/2)**2/10)
    rho0 = N*rho0/NLS.integral(rho0,eps)

    gamma1, listgamma1, listenergy1 = NLS.find_best_gamma(rho0, N, p, eps, tol=1e-10, verbose = 0, sparse=False)
    energy = listenergy1[-1]
    ans.append(energy)
    list_rho.append( NLS.gamma2rho(gamma1) )
    print("\tNumber of iterations = ", len(listgamma1))
    print("\tEnergy = ", energy)

##################################

