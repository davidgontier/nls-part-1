# In this code, we solve the fermionic NLS problem
# We minimise the function 
# $$
#   E(\gamma) := {\rm Tr}( - \Delta \gamma) - \frac1p \int \rho^p.
# $$
# in one dimension.

import numpy as np
import scipy.sparse.linalg as LA

class Gamma:
    def __init__(self, gamma, eigs):
        self.gamma = gamma
        self.eigs = eigs
        self.lam = sum(eigs) #number of particles
        self.Ng = np.shape(gamma)[0]
        self.Np1 = np.shape(gamma)[1]


def integral(f, eps): return sum(f)*eps

def gamma2rho(gamma, eigs = None):
    # to get rho from gamma
    # Here, gamma is a NgxN matrix, where N is the number of electrons, and Ng the number of points in the grid
    Ng = np.shape(gamma.gamma)[0] #Here, N is the number of electrons, or the number plus 1 (if eigs is provided)
    rho = np.dot(gamma.gamma**2, gamma.eigs)

    # to center rho
    i = np.argmax(rho)
    return np.roll(rho,Ng//2-i)

###############################################
# The energy
###############################################
def kinetic_energy(gamma,eps):
    kinetic=0
    for i in range(gamma.Np1):
        ui = gamma.gamma[:,i]
        nabla_ui = (ui - np.roll(ui,1))/eps
        kinetic += gamma.eigs[i] * integral( nabla_ui**2, eps )
    return kinetic

def potential_energy(rho, p, eps): return -integral(rho**p, eps)/p

def energy_rHF(gamma,p, eps):
    rho = gamma2rho(gamma)
    return kinetic_energy(gamma ,eps)+potential_energy(rho, p, eps)

###############################################
# The Hamiltonian
###############################################
def get_H_rho(rho, p, eps): #here is full matrix. Sparsity is possible
    Ng = np.shape(rho)[0]
    kinetic = (2*np.eye(Ng) - np.roll(np.eye(Ng),1,axis=1) - np.roll(np.eye(Ng),-1, axis=1))/eps**2
    potential=-np.diag(rho**(p-1))
    return kinetic+potential

def multH_rho(rho, p, eps, u): 
    #returns the multiplication Hu
    return (2*u - np.roll(u,1) - np.roll(u,-1))/eps**2 - rho**(p-1)*u


##############################################
# The minimisation procedure
##############################################

def find_best_gamma(rho0, lam, p, eps, tol=1e-7, Niter=1000, verbose = 1, sparse = True):
    assert abs( integral(rho0, eps) - lam) < 1e-8, "Wrong number of particle in rho0"

    listgamma = []
    listenergy = []
    Ng = np.shape(rho0)[0]
 
    N = int ( lam ) # integer part of the electrons
    delta = lam - N  # non integer part, between 0 and 1
    
    eigs = np.ones(N+1)
    eigs[-1] = delta
    
    rho_n, energy_n = rho0, float('inf')
    if verbose:
        print("Iteration\tEnergy\n----------\t-----------")
    # main loop
    for n in range(Niter):
        #sparse method,... not yet efficient (why???)
        if sparse:
            def multH(u): 
                return multH_rho(rho_n, p, eps, u)
            H = LA.LinearOperator((Ng, Ng), matvec = multH)
            W, V = LA.eigsh(H, N+1, which = 'SA') # sparse method
        
        #full method
        else:
            H = get_H_rho(rho_n, p, eps)
            W,V = np.linalg.eigh(H)

        gamma_n = Gamma(V[:,0:N+1]/np.sqrt(eps) , eigs ) #normalised vector, so integral = 1
        rho_n = gamma2rho(gamma_n, eigs)
        energy_np1 = energy_rHF(gamma_n, p, eps)
        if verbose:
            print(n, "\t\t", energy_np1)
        
        listgamma.append(gamma_n)
        listenergy.append(energy_np1)
        
        if abs(energy_np1 - energy_n) < tol:
            return gamma_n, listgamma, listenergy
        energy_n = energy_np1
        
    print("did not converge")
    return gamma_n, listgamma, listenergy

##########################################################


if __name__ == "__main__":
    # The case N = 1
    L = 50 #computation on the segment [O, L]
    Ng = 500 # number of grid points in [0, L]
    N = 2 # number of electrons
    p = 1.3 # the p value
    d = 8 #space between electrons
    ############################
    eps = L/Ng
    xx = np.linspace(0, L-eps, Ng) # the grid

    rho0 = np.zeros(Ng)
    for i in range(N):
        rho0 += np.exp(-(xx+i*d-3*L/4)**2/10)
    rho0 = N*rho0/integral(rho0,eps)

    gamma1, listgamma1, listenergy1 = find_best_gamma(rho0, N, p, eps, tol=1e-10, verbose = 1, sparse=False)
    rho1 = gamma2rho(gamma1)

    print("Convergence in ", len(listgamma1), "iterations.")

    # Check viriel
    print("kinetic=", kinetic_energy(gamma1, eps))
    print("potential=", integral(rho1**p, eps))
    print("should be null = ", kinetic_energy(gamma1, eps) - (p-1)*1/2/p*integral(rho1**p, eps))