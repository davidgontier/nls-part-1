import numpy as np
import scipy.interpolate as interpolate
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))

# In this file we record many constants (and pre-computed constant functions)
pi = np.pi

################################
# The constants
################################
dd  = np.array([1, 1, 2, 3]) # the dimension. dd[0] to avoid zeros problem

SS = np.array([2, 2, 2*pi, 4*pi]) # SS[d] is the volume of the $d$ sphere
BB = np.array([2, 2, pi, 4/3*pi]) # BB[d] is the volume of the $d$ ball

L_sc = 2/(dd+2)*BB/(2*pi)**dd
L_LT = 1.456*L_sc
K_sc = dd/(dd+2)*(2*pi)**2/BB**(2/dd)
K_LT = K_sc*(1/1.456)**(2/dd) # The Lieb-Thirring constant, from [Frank, Hundertmark, Jex, Nam] Proposition 10.
K_LT[1] = K_sc[1]*0.471851 # better constant in dimension 1.


##################################
# dimension d = 1
##################################
# The function I1p returns the bosonic value I1(d, p). Here for d = 1.
# the values are pre-computed for all 1 < p <= 2

I1p = np.load(dir_path+"/data/I1p.npy").item()
I1 = interpolate.interp1d(I1p["prange"], I1p["I1"])

I2p = np.load(dir_path+"/data/I2p.npy").item()
I2 = interpolate.interp1d(I2p["prange"], I2p["I2"])

I3p = np.load(dir_path+"/data/I3p.npy").item()
I3 = interpolate.interp1d(I3p["prange"], I3p["I3"])

def Id(d, p):
    # to avoid problems with 0
    if d==1: return np.minimum( I1(p), 0)
    if d==2: return np.minimum( I2(p), 0)
    if d==3: return np.minimum( I3(p), 0)

def UB(d, p): # the lower bound for J(N)/N in Eqt (12) of our article
    return -(1+2/d-p)*d/(2*p)*( d*(p-1)/(2*p*K_sc[d]) )**( (p-1)/(1+2/d - p) )

def LB(d, p): # the lower bound for J(N)/N in Eqt (12) of our article
    return -(1+2/d-p)*d/(2*p)*( d*(p-1)/(2*p*K_LT[d]) )**( (p-1)/(1+2/d - p) )

def viriel(d, p): 
    # kinetic = viriel1*potential
    return d*(p-1)/(2*p)

def ratio(d, p):
    # energy = ratio * Tr(H \gamma)
    return (viriel(d, p) - 1)/(viriel(d, p) - p)
