import numpy as np
import NLS_1d as NLS

##################################
# Fixed Parameters
##################################

L = 300 #computation on the segment [O, L]
Ng = 5000 # number of grid points in [0, L]
p = 1.3 # the p value

eps = L/Ng
xx = np.linspace(0, L-eps, Ng) # the grid
d = 8

NN = range(1, 21)
energyN = []
rhoN = []

####################################
# Echo parameters
print("Echo parameters")
print("L = ", L)
print("Ng = ", Ng)
print("p = ", p)
print("d = ", d)
print("NN = ", NN)
print("____________________________\n")

###################################
for N in NN:
    print("\n\nComputation for N = ", N)

    # constructing first guess
    rho0 = np.zeros(Ng)

    for i in range(N):
        rho0 += np.exp(-(xx+i*d-3*L/4)**2/10)
    rho0 = N*rho0/NLS.integral(rho0,eps)

    gamma1, listgamma1, listenergy1 = NLS.find_best_gamma(rho0, N, p, eps, tol=1e-10, verbose = 1, sparse=False)

    energy = listenergy1[-1]
    energyN.append(energy)
    rhoN.append( NLS.gamma2rho(gamma1) )
    #print("\tNumber of iterations = ", len(listgamma1))
    print("\n For N = ", N, "Energy = ", energy)

##################################
np.save("save_rho.npy", rhoN)
