####################################
# This code is a finite element one to compute the best 2d solution
# of the NLS problem :
# Tr(- \Delta \gamma) - 1/p \int \rho^p
#
# Written by D. Gontier on December 11 2019.

import numpy as np
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import os
import datetime 
from utils import *


###############################
# The parameters
##############################

Lx, Ly, eps = 301, 301, 0.5
prange = np.linspace(1.1,1.9,9)


#############################
# We record the results in a new folder
#############################
now = datetime.datetime.now()
dirname = "./data/%d_%d_%d%d/"%(now.month, now.day, now.hour, now.minute)
os.makedirs(dirname)
filename = dirname+"results.md"

myprint(filename, "Record in directory: " + dirname + " and in file " + filename)


ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)

# First rho0
xgrid, ygrid = np.meshgrid(xx, yy)
def f_exp(x,y): return np.exp(-(x**2 + y**2)/20)
rho0 = f_exp(xgrid, ygrid)
rho0 /= integral(rho0, eps)

for p in prange:
    #Computation for 1 electron
    nelec = 1
    myprint(filename, "\n\n###############################")
    myprint(filename, "Computation for nelec = " + str(nelec) + " and p = " + str(p))
    rho1, I1 = find_best_gamma(rho0, nelec, p, eps, tol=1e-8, Niter=30)

    # save result
    res = {
        "p": p,
        "Lx" : Lx,
        "Ly" : Ly,
        "eps": eps,
        "rho1": rho1,
        "I1": I1
    }
    np.save(dirname+"res_n1_p"+str(int(10*p)), res)

    # plot result
    plt.figure()
    plt.title("p = "+str(p))
    plt.contourf(xx,yy,rho1)
    plt.axis("equal")
    plt.colorbar()
    plt.savefig(dirname+"fig_n1_p"+str(int(10*p))+".png", bbox_inches='tight')

    #Update initialisation
    rho0 = rho1
    