####################################
# This code creates the data for p = 1.3 and several N

import numpy as np
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import os
import sys
import datetime
sys.path.append('../')
import multiprocessing
from multiprocessing import Pool

from utils import *

###############################
# The global parameters
##############################

show = True
compute = False
Lx, Ly, eps = 151, 151, 0.3
p = 1.3
filename = "a.out"

#############################
# We record the results in a new folder
#############################

ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)


def run(nelec):
    # We load the coarse computation,
    # We interpolate to have the starting point
    myprint(filename, "\n Computation for n = %d"%nelec)

    res = np.load("./N%d.npy"%nelec, allow_pickle=True).item()
    Lx_c, Ly_c, eps_c = res['Lx'], res['Ly'], res['eps']

    ax_c, ay_c = (Lx_c-1)*eps_c/2, (Ly_c-1)*eps_c/2
    xx_c, yy_c = np.linspace(-ax_c, ax_c, Lx_c), np.linspace(-ay_c, ay_c, Ly_c)

    rr = interpolate.RectBivariateSpline(xx_c, yy_c, res['rho'])
    rho0 = rr(xx, yy) # in a finer grid

    if compute:
        rho, I = find_best_gamma(rho0, nelec, p, eps, tol=1e-8, Niter=200)
        myprint(filename, "For n = %d, we have I = %f"%(nelec,I))

        #record_result
        N = {
            "nelec": nelec,
            "rho" : rho,
            "I" : I,
            "Lx" : Lx,
            "Ly" : Ly,
            "eps" : eps,
            "p" : p
        }
        myprint(filename, "Saving file in ./N%d.npy"%nelec)
        np.save("./N%d"%nelec, N)
    else:
        rho = rho0

    if show:
        plt.contourf(xx, yy, rho, cmap='YlGnBu', levels=100)
        plt.axis("equal")
        plt.title("p = %.2f, N = %d"%(p, nelec))
        myprint(filename, "Saving file p%dN%d.png"%(int(10*p), nelec))
        plt.savefig("p%dN%d.png"%(int(10*p), nelec), bbox_inches='tight')
        #plt.show()
    return


################################
if __name__ == "__main__":

    now = datetime.datetime.now()
    myprint(filename, "Computation starts on %d/%d/%d at %d:%d"%(now.month, now.day, now.year, now.hour, now.minute))


    Ncpu = multiprocessing.cpu_count()
    myprint(filename, "Number of cpu = %d"%Ncpu)
    
    # The main loop in multiprocessing
    #with Pool(Ncpu-3) as pool:
    #    pool.map(run, range(1,7))
    for nelec in range(1,7):
        run(nelec)
    myprint(filename, "Done.")
    now = datetime.datetime.now()
    myprint(filename, "Computation ends on %d/%d/%d at %d:%d"%(now.month, now.day, now.year, now.hour, now.minute))


# Gives the following result
# For n = 1, we have I = -0.121496
# For n = 2, we have I = -0.245709
# For n = 3, we have I = -0.374078
# For n = 4, we have I = -0.500203
# For n = 5, we have I = -0.628102
# For n = 6, we have I = -0.757187
# Done.