####################################
# This code is a finite element one to compute the best LT constant in 2D
# It is inspired by the code by Antoine Levitt
#
# Written by D. Gontier on December 11 2019.

import numpy as np
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import scipy.special as special

#####################################
# We work on a grid of size Ly x Lx. The step size is eps in both direction



def integral(u, eps): return np.sum(u)*eps**2

# gamma only contains the eigenvectors (not the projection)
# it is of size nelec x Ly x Lx
def gamma2rho(gamma):
    return np.sum(gamma**2, axis=0)



###############################
# COMPUTATION OF THE HAMILTONIAN
###############################
# The grid is huge, so we need to work with sparse matrices

def get_mDelta(Lx, Ly, eps, scale=1): #minus Laplacian
    def mult_mDelta(ul): #ul is a line of size Ly x Lx
        u       = np.reshape(ul, (Ly,Lx)) # the first dimension is y
        dxu     = (u - np.roll(u,1,axis=1))/eps
        ddxu    = (dxu - np.roll(dxu,-1, axis=1))/eps
        dyu     = (u - np.roll(u,1,axis=0))/eps
        ddyu    = (dyu - np.roll(dyu,-1, axis=0))/eps
        mDeltau = ddxu + ddyu
        return scale**2*np.reshape(mDeltau, (Lx*Ly))

    return LA.LinearOperator((Lx*Ly, Lx*Ly), matvec=mult_mDelta)

def get_V_LT(gamma, kappa, Itarget, eps):
    # return a potential V such that 
    # \int V^{\kappa + 1}
    # V = cst.rho^{1/kappa}, and the constant is chosen to ensure int = I
    rho = gamma2rho(gamma)
    V = rho**(1/kappa)
    I = integral(V**(kappa+1), eps)
    return V*(Itarget/I)**(1/(kappa+1)) # normalisation

def get_H_LT(V, Lx, Ly, eps):
    # create multiplication operator
    def mult_Vrho(ul):
        u = np.reshape(ul, (Ly,Lx))
        Vu = -V*u
        return np.reshape(Vu, (Lx*Ly))
    Vop = LA.LinearOperator((Lx*Ly, Lx*Ly), matvec=mult_Vrho)
    H = get_mDelta(Lx, Ly, eps) + Vop
    return H

def get_gamma_LT(V, kappa, Lx, Ly, eps, nelec):
    H = get_H_LT(V, Lx, Ly, eps)
    neigs = np.maximum( 5, 2*nelec  )
    w, v = LA.eigsh(H, neigs, which='SA')
    
    nnegval = sum(w<0)
    if nnegval < nelec:
        print("Error, not enough negative eigenvalues")
        print("nnegval = ", nnegval)
        print("w = ", w)
        print("w<0=", w<0)
        return

    sumEigs = sum(abs(w[w<0])**kappa)
    
    gamma = np.zeros((nnegval, Ly, Lx))
    for i in range(nnegval):
        eigval = abs(w[i])**(kappa-1)
        gamma[i, :, :] = eigval*np.reshape(v[:,i], (Ly, Lx))/eps
    return gamma, sumEigs

#####################################
# THE MINIMISATION PROCEDURE
#####################################

def get_Lsc(kappa):
    return 1/(4*np.pi)*special.gamma(kappa+1)/special.gamma(kappa+2) #semi classical cst

def find_best_LT(V0, kappa, Lx, Ly, eps, nelec, tol = 1e-7, Niter = 10):
    Lsc = get_Lsc(kappa)
    kp = kappa/(kappa - 1)
    p = (1+kappa)/kappa

    # find magic factor beta
    #rho0 = V0**(1/(p-1))
    #Ip = integral(rho0**p, eps)
    #print("Ip = ", Ip)
    #beta = (kappa/(kappa + 1)*Ip**(1 - 1/kappa)/nelec**(1/kp) )**kappa
    #V0 = beta*V0 # rescale V0

    I0 = integral(V0**(kappa+1), eps)
    print("--------------------")
    print("kappa = %f, p = %f, kp = %f"%(kappa, p, kp))
    print("I0 = ", I0)
    print('---------------------')
    print("|    Lsc = %f"%Lsc)
    print('---------------------')
    


    Vn = V0
    Ln = -float('inf')
    for i in range(Niter):
        gamman, sumEigs = get_gamma_LT(Vn, kappa, Lx, Ly, eps, nelec)
        Vnp1 = get_V_LT(gamman, kappa, I0, eps)
        #print("must be null:", integral(Vn**(kappa+1), eps)-I0)
        Lnp1 = sumEigs/I0
        print("i = %d, neigs = %d, L > %f, ratio = %f"%(i,np.shape(gamman)[0],Lnp1, Lnp1/Lsc))

        # check if improvement or tolerance reached
        if Lnp1 < Ln + tol: 
            return Vn, Ln
        Vn, Ln = Vnp1, Lnp1
    return Vnp1, Lnp1

def get_initial_V0(nelec, xx, yy):
     #load initial value
    res = np.load("init_for_LT_%d.npy"%nelec).item()

    # the coarse grid
    Lx_c, Ly_c, eps_c = res['Lx'], res['Ly'], res['eps']
    ax_c, ay_c = (Lx_c-1)*eps_c/2, (Ly_c-1)*eps_c/2
    xx_c, yy_c = np.linspace(-ax_c, ax_c, Lx_c), np.linspace(-ay_c, ay_c, Ly_c)


    V = interpolate.RectBivariateSpline(xx_c, yy_c, res['V'])
    return V(yy, xx) # in a finer grid


###############################
# OTHER
###############################

# To print the results
def myprint(filename, s):
    print(s)
    with open(filename, "a") as file:
        file.write("\n" + s)
##########################

if __name__ == "__main__":
    # the parameters
    nelec = 1
    Lx, Ly, eps = 200, 200, 0.1
    kappa = 1.17

    # Echo the parameters
    print("############################")
    print("This is main_LiebThirring")
    print("Lx = %d, Ly = %d, eps = %f, nelec = %d, kappa = %f"%(Lx, Ly, eps, nelec, kappa))
    print("############################\n")

    ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
    xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)

    #load initial value
    V0 = get_initial_V0(nelec, xx, yy)

    Vn, Ln = find_best_LT(V0, kappa, Lx, Ly, eps, nelec, Niter = 15)
    
    print("Final guess = ", Ln)
    # plot figure
    ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
    xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)
    plt.figure(figsize = (8,8))
    plt.contourf(xx, yy, Vn)
    plt.show()
    plt.savefig("LT_kappa%d_N%d.png"%(int(1000*kappa), nelec))