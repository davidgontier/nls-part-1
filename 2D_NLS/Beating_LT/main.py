
import numpy as np
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import scipy.special as special
import multiprocessing
from multiprocessing import Pool
import datetime

from utils_LiebThirring import *


# the parameters
Lx, Ly, eps, Niter = 100, 80, 0.5, 30

Nkappa = 10
nelecRange = [1,3, 6, 10]
kappaRange = np.linspace(1.15, 1.3, Nkappa)

compute, show = True, True

# Echo the parameters
print("############################")
print("This is main.py for Lieb Thirring")
print("Lx = %d, Ly = %d, eps = %f, Nkappa = %d"%(Lx, Ly, eps, Nkappa))
print("############################\n")

ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)

def run(nelec):
    
    #load initial value
    V0 = get_initial_V0(nelec, xx, yy)
    Lrecord = []
    Vrecord = []
    for kappa in kappaRange:
        Vn, Ln = find_best_LT(V0, kappa, Lx, Ly, eps, nelec, Niter = 15)
        Lrecord.append(Ln)
        Vrecord.append(Vn)
        V0 = Vn

        #if show:
        fig = plt.figure( )
        plt.contourf(xx, yy, Vn, cmap='YlGnBu', levels=100)
        plt.title("N = %d, kappa = %f"%(nelec, kappa))
        plt.axis('equal')
        plt.savefig("images_N%d/kappa%d.png"%(nelec, int(1000*kappa)), bbox_inches='tight')
        plt.close(fig)
    #record result
    res = {
        "kappaRange": kappaRange,
        "Vrecord": Vrecord,
        "Lrecord": Lrecord,
        "Lx": Lx, "Ly": Ly, "eps": eps
    }
    np.save("res_N%d"%nelec, res)

    return Lrecord

if __name__ == "__main__":
    now = datetime.datetime.now()
    print("Computation starts on %d/%d/%d at %d:%d"%(now.month, now.day, now.year, now.hour, now.minute))

    Ncpu = multiprocessing.cpu_count()
    print("Number of cpu = %d"%Ncpu)
    
    #Computation
    if compute:
        #with Pool(Ncpu-2) as pool:
        #    result = pool.map(run, nelecRange)

        result = []
        for nelec in nelecRange:
            print("\n\nComputation for nelec = ", nelec)
            result.append(run(nelec))

        print("Done!\n\n")

    # plot result
    if show:
        plt.figure( figsize=(8,8) )
        for i in range(len(nelecRange)):
            plt.plot(kappaRange, result[i], label='nelec = %d'%(nelecRange[i]))
        # add the semiclassical
        Lsc = [get_Lsc(kappa) for kappa in kappaRange]
        plt.plot(kappaRange, Lsc, ":k", label="Lsc")
        plt.legend()
        #
        plt.savefig("results.png", bbox_inches='tight')
        #fig.show()

    



