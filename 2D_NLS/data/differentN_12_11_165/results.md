
Record in directory: ./data/differentN_12_11_165/ and in file ./data/differentN_12_11_165/results.md


###############################
Computation for nelec = 1 and p = 1.3
energy = -0.12155675356309825


###############################
Computation for nelec = 2 and p = 1.3
energy = -0.24582914831511668


###############################
Computation for nelec = 3 and p = 1.3
energy = -0.3742321495092933


###############################
Computation for nelec = 4 and p = 1.3
energy = -0.49724430281299276