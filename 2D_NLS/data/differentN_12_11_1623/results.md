
Record in directory: ./data/differentN_12_11_1623/ and in file ./data/differentN_12_11_1623/results.md


###############################
Computation for nelec = 1 and p = 1.3
energy = -0.121556754755133


###############################
Computation for nelec = 2 and p = 1.3
energy = -0.24582919367267833


###############################
Computation for nelec = 3 and p = 1.3
energy = -0.37422887208831696


###############################
Computation for nelec = 4 and p = 1.3
energy = -0.4972446673369607