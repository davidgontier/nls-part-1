####################################
# This codes compute the best N = 2, for different p's

import numpy as np
import scipy.sparse.linalg as LA
import scipy.interpolate as interpolate
import scipy.special as special
import os
import sys
import datetime
sys.path.append('../')

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from utils import *

###############################
# The global parameters
##############################

compute = True
show = True
# parameters
Lx, Ly, eps = 101, 101, 0.5
ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)



filename = "a.out"

Ncomputations = 3
prange = np.linspace(1.6, 1.7, Ncomputations)
srange = np.linspace(0.5, 0.15, Ncomputations)
Niter = 20

nelec = 1

myprint(filename, "Computation of the initialisation")
# First rho0, a gaussian
xgrid, ygrid = np.meshgrid(xx, yy)
def f_exp(x,y): return np.exp(-(x**2 + y**2)/20)
rho0 = f_exp(xgrid, ygrid)
rho0 /= integral(rho0, eps)


def run(i):
    p,s = prange[i], srange[i]

    myprint(filename, "\n######################\nComputation for p = %f and s = %f"%(p, s))
    myprint(filename, "\nFirst, we compute for N = 1")
    nelec = 1
    rho1, I1 = find_best_gamma(rho0, nelec, p, eps, scale = s, tol=1e-8, Niter=Niter)


    rho2_0 = np.roll(rho1, -10, axis = 1) + np.roll(rho1, 10, axis = 1)
    #ho3_0 = rho2_0 + np.roll(rho1, int(24*np.sqrt(3)/2), axis = 0)

    myprint(filename, "\nWe now compute for N = 2")
    nelec = 2
    rho2, I2 = find_best_gamma(rho2_0, nelec, p, eps, scale = s, tol=1e-8, Niter=Niter)

    gain = (I2 - 2*I1)/(2*I1)
    myprint(filename, "For p = %f and s = %f :"%(p, s))
    myprint(filename, "I1 = %f and I2 = %f"%(I1, I2))
    myprint(filename, "The gain is in percent: %f"%gain)

    #record_result
    res = {
        "nelec": nelec,
        "rho1" : rho1,
        "I1" : I1,
        "rho2" : rho2,
        "I2" : I2,
        "Lx" : Lx,
        "Ly" : Ly,
        "eps" : eps,
        "p" : p,
        "s" : s,
        }
    myprint(filename, "Saving file in ./p%dN2.npy"%int(100*p))
    np.save("./p%dN2"%int(100*p), res)

    if show:
        plt.figure()
        plt.contourf(xx, yy, rho1, cmap='YlGnBu', levels=100)
        plt.axis("equal")
        plt.title("p = %.3f, N = 1"%p)
        myprint(filename, "Saving file p%dN1.png"%(int(100*p)))
        plt.savefig("p%dN1.png"%int(100*p), bbox_inches='tight')

        plt.figure()
        plt.contourf(xx, yy, rho2, cmap='YlGnBu', levels=100)
        plt.axis("equal")
        plt.title("p = %.3f, N = 2"%p)
        myprint(filename, "Saving file p%dN2.png"%(int(100*p)))
        plt.savefig("p%dN2.png"%int(100*p), bbox_inches='tight')
        #plt.show()
 
    ###############################
    # Check Lieb-Thirring
    myprint(filename, "\nCheck Lieb-Thirring")
    V2 = 1/s**(2*(2-p))*rho2**(p-1)

    def mult_Vrho(ul):
        u = np.reshape(ul, (Ly,Lx))
        Vu = -V2*u
        return np.reshape(Vu, (Lx*Ly))
    Vop = LA.LinearOperator((Lx*Ly, Lx*Ly), matvec=mult_Vrho)

    H = get_mDelta(Lx, Ly, eps) + Vop

    neigs = 10
    w, v = LA.eigsh(H, neigs, which='SA')

    kappa = p/(p-1) - 1
    Lsc = 1/(4*np.pi)*special.gamma(kappa+1)/special.gamma(kappa+2)

    sumEigs = np.sum(np.abs(w[w<0])**kappa)
    intV3 = integral(V2**(kappa+1), eps)
    myprint(filename, "sumEigs = %f, and intVkappa = %f"%(sumEigs, intV3))
    myprint(filename, "kappa = %f and Lsc = %f"%(kappa, Lsc))
    myprint(filename, "And we find L > %f"%(sumEigs/intV3))
    return

###################################
if __name__ == "__main__":
    for i in range(Ncomputations):
        run(i)
    myprint(filename, "Done!")