####################################
# This codes compute the best N = 3, for different p's

import numpy as np
import scipy.sparse.linalg as LA
import scipy.interpolate as interpolate
import scipy.special as special
import os
import sys
import datetime
sys.path.append('../')

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import multiprocessing
from multiprocessing import Pool

from utils import *

###############################
# The global parameters
##############################

compute = True
show = True
# parameters
Lx, Ly, eps = 101, 101, 0.5
ax, ay = (Lx-1)*eps/2, (Ly-1)*eps/2 #size of the box
xx, yy = np.linspace(-ax, ax, Lx), np.linspace(-ay, ay, Ly)


filename = "a.out"

Ncomputations = 3
prange = np.linspace(1.6, 1.7, Ncomputations)
srange = np.linspace(0.5, 0.12, Ncomputations)
Niter = 100


myprint(filename, "Computation of the initialisation")
# First rho0, a gaussian
xgrid, ygrid = np.meshgrid(xx, yy)
def f_exp(x,y): return np.exp(-(x**2 + y**2)/20)
rho0 = f_exp(xgrid, ygrid)
rho0 /= integral(rho0, eps)


def run(i):
    p,s = prange[i], srange[i]

    myprint(filename, "\n\n######################\nComputation for p = %f and s = %f"%(p, s))
    myprint(filename, "\nFirst, we compute for N = 1")
    nelec = 1
    rho1, I1 = find_best_gamma(rho0, nelec, p, eps, scale = s, tol=1e-8, Niter=Niter)


    rho2_0 = np.roll(rho1, -10, axis = 1) + np.roll(rho1, 10, axis = 1)
    rho3_0 = rho2_0 + np.roll(rho1, int(24*np.sqrt(3)/2), axis = 0)

    myprint(filename, "\nWe now compute for N = 3")
    nelec = 3
    rho3, I3 = find_best_gamma(rho3_0, nelec, p, eps, scale = s, tol=1e-8, Niter=Niter)

    gain = (I3 - 3*I1)/(3*I1)
    myprint(filename, "For p = %f and s = %f :"%(p, s))
    myprint(filename, "I1 = %f and I3 = %f"%(I1, I3))
    myprint(filename, "The gain is in percent: %f"%gain)

    #record_result
    res = {
        "nelec": nelec,
        "rho1" : rho1,
        "I1" : I1,
        "rho3" : rho3,
        "I3" : I3,
        "Lx" : Lx,
        "Ly" : Ly,
        "eps" : eps,
        "p" : p,
        "s" : s,
        }
    myprint(filename, "Saving file in data/N3p%d.npy"%int(100*p))
    np.save("data/N3p%d"%int(100*p), res)

    if show:
        plt.figure()
        plt.contourf(xx/s, yy/s, rho1, cmap='YlGnBu', levels=100)
        plt.axis("equal")
        plt.title("p = %.3f, N = 1"%p)
        myprint(filename, "Saving file Images_N1/N1p%d.png"%(int(100*p)))
        plt.savefig("Images_N1/N1p%d.png"%int(100*p), bbox_inches='tight')

        plt.figure()
        plt.contourf(xx/s, yy/s, rho3, cmap='YlGnBu', levels=100)
        plt.axis("equal")
        plt.title("p = %.3f, N = 3"%p)
        myprint(filename, "Saving file Images_N3/N3p%d.png"%(int(100*p)))
        plt.savefig("Images_N3/N3p%d.png"%int(100*p), bbox_inches='tight')
        #plt.show()
 
    ###############################
    # Check Lieb-Thirring
    myprint(filename, "\nCheck Lieb-Thirring")
    V3 = 1/s**(2*(2-p))*rho3**(p-1)

    def mult_Vrho(ul):
        u = np.reshape(ul, (Ly,Lx))
        Vu = -V3*u
        return np.reshape(Vu, (Lx*Ly))
    Vop = LA.LinearOperator((Lx*Ly, Lx*Ly), matvec=mult_Vrho)

    H = get_mDelta(Lx, Ly, eps) + Vop

    neigs = 10
    w, v = LA.eigsh(H, neigs, which='SA')

    kappa = p/(p-1) - 1
    Lsc = 1/(4*np.pi)*special.gamma(kappa+1)/special.gamma(kappa+2)

    sumEigs = np.sum(np.abs(w[w<0])**kappa)
    intV3 = integral(V3**(kappa+1), eps)
    myprint(filename, "sumEigs = %f, and intVkappa = %f"%(sumEigs, intV3))
    myprint(filename, "kappa = %f and p = %f"%(kappa, p))
    myprint(filename, "The true L is > %f, and Lsc = %f"%(sumEigs/intV3, Lsc))
    return

###################################
if __name__ == "__main__":
    now = datetime.datetime.now()
    myprint(filename, "Computation starts on %d/%d/%d at %d:%d"%(now.month, now.day, now.year, now.hour, now.minute))


    Ncpu = multiprocessing.cpu_count()
    myprint(filename, "Number of cpu = %d"%Ncpu)
    
    # The main loop in multiprocessing
    with Pool(Ncpu) as pool:
        pool.map(run, range(Ncomputations))
    #for i in range(Ncomputations):
    #    run(i)
    myprint(filename, "Done.")
    now = datetime.datetime.now()
    myprint(filename, "Computation ends on %d/%d/%d at %d:%d"%(now.month, now.day, now.year, now.hour, now.minute))
