####################################
# This code is a finite element one to compute the best 2d solution
# of the NLS problem :
# Tr(- \Delta \gamma) - 1/p \int \rho^p
#
# Written by D. Gontier on December 11 2019.

import numpy as np
import scipy.sparse.linalg as LA


#####################################
# We work on a grid of size Ly x Lx. The step size is eps in both direction


#############################
# COMPUTATION OF THE ENERGY
#############################
# we add a scale parameters
# the energy is s**2( Tr(-\Delta \gamma) - 1/(p*s^{4-2p}) \int \rho^p)
# So we have energy(\gamma, 1) = energy(\gamma_s, s)
# with \gamma_s = s^{-2} \gamma(x/s, y/s)

def integral(u, eps): return np.sum(u)*eps**2

# gamma only contains the eigenvectors (not the projection)
# it is of size nelec x Ly x Lx
def gamma2rho(gamma):
    return np.sum(gamma**2, axis=0)

# The code is periodic. We compute the kinetic energy with finite differences
def kinetic_energy(gamma, eps, scale=1):
    kinetic = 0
    Nelec = np.shape(gamma)[0]
    for i in range(Nelec):
        ui = gamma[i,:,:]
        dxui = (ui - np.roll(ui,1, axis=1))/eps
        dyui = (ui - np.roll(ui,1, axis=0))/eps
        kinetic += integral(dxui**2 + dyui**2, eps)
    return scale**2*kinetic

def potential_energy(rho, p, eps, scale=1): 
    return -integral(rho**p, eps)/p/scale**(2 - 2*p)

def energy(gamma, p, eps, scale=1): 
    rho = gamma2rho(gamma)
    return kinetic_energy(gamma, eps, scale) + potential_energy(rho, p, eps, scale)

def viriel_energy(gamma, p, eps, scale=1):
    rho = gamma2rho(gamma)
    kinetic = kinetic_energy(gamma, eps, scale)
    potential = potential_energy(rho, p, eps, scale)
    return kinetic + potential*(p-1)


###############################
# COMPUTATION OF THE HAMILTONIAN
###############################
# The grid is huge, so we need to work with sparse matrices

def get_mDelta(Lx, Ly, eps, scale=1): #minus Laplacian
    def mult_mDelta(ul): #ul is a line of size Ly x Lx
        u       = np.reshape(ul, (Ly,Lx)) # the first dimension is y
        dxu     = (u - np.roll(u,1,axis=1))/eps
        ddxu    = (dxu - np.roll(dxu,-1, axis=1))/eps
        dyu     = (u - np.roll(u,1,axis=0))/eps
        ddyu    = (dyu - np.roll(dyu,-1, axis=0))/eps
        mDeltau = ddxu + ddyu
        return scale**2*np.reshape(mDeltau, (Lx*Ly))

    return LA.LinearOperator((Lx*Ly, Lx*Ly), matvec=mult_mDelta)

def get_Vrho(rho, p, scale=1): # The multiplication operator by \rho^{p-1}
    Ly, Lx = np.shape(rho)
    def mult_Vrho(ul):
        u = np.reshape(ul, (Ly,Lx))
        Vu = -rho**(p-1)*u
        return 1/scale**(2-2*p)*np.reshape(Vu, (Lx*Ly))
    return LA.LinearOperator((Lx*Ly, Lx*Ly), matvec=mult_Vrho)

def get_H(rho, p, eps, scale=1):
    Ly, Lx = np.shape(rho)
    return get_mDelta(Lx,Ly,eps, scale) + get_Vrho(rho, p, scale)

def get_projector(rho, nelec, p, eps, scale=1): #the projector of H
    Ly, Lx = np.shape(rho)
    H = get_H(rho, p, eps, scale)
    w, v = LA.eigsh(H, nelec, which='SA') # sparse eigenvalue problem
    # TODO: add a preconditionner?
    
    gamma = np.zeros((nelec, Ly, Lx))
    for n in range(nelec): 
        #normalised so that \int u^2 = 1
        gamma[n,:,:] = np.reshape(v[:,n], (Ly,Lx))/eps 
    return gamma

#####################################
# THE MINIMISATION PROCEDURE
#####################################

def find_best_gamma(rho0, nelec, p, eps, scale = 1, tol=1e-5, Niter=30):
    # rho0 is the initial density
    # nelec is the number of desired electrons (here, usually 1 or 2)

    Ly, Lx = np.shape(rho0)

    # echo variables
    print("Find best gamma with the following parameters:")
    print("nelec = ", nelec)
    print("p = ", p)
    print("Lx, Ly, eps, scale = ", Lx, Ly, eps, scale)
    print("tol, Niter = ", tol, Niter)
    
    # Main loop
    rhon, energyn = rho0, float('inf')
    #list_gamma, list_energy = [], [energy_n]

    for i in range(Niter):
        gamman = get_projector(rhon, nelec, p, eps, scale)
        rhon =  gamma2rho(gamman)
        energynp1 = energy(gamman, p, eps, scale)
        
        #list_gamma.append(gamma_n)
        #list_energy.append(energy_np1)
        
        kinetic = kinetic_energy(gamman, eps, scale)
        potential = potential_energy(rhon, p, eps, scale)
        viriel = viriel_energy(gamman, p, eps, scale)
        
        print("\nIteration ", i)
        print("\tNormalised Energy = ", energynp1/nelec)
        print("\tkinetic = ", kinetic, "potential = ", potential)
        print("\tViriel = ", viriel) # here for d = 2
        
        if energynp1 > energyn:
            print("Problem, the energy is higher...")
            
        if abs(energynp1 - energyn) < scale**2*tol:
            return rhon, energynp1
        
        energyn = energynp1
    print("Did not converge after ", Niter, " iterations?")
    return rhon, energynp1

###############################
# OTHER
###############################

# to find the distance between the two electrons, we need to find the local maxima
def find_maxima(rho):
    maxx = rho > np.maximum(np.roll(rho, 1, axis=1), np.roll(rho, -1, axis=1))
    maxy = rho > np.maximum(np.roll(rho, 1, axis=0), np.roll(rho, -1, axis=0))
    return np.nonzero(np.logical_and(maxx, maxy))

# To print the results
def myprint(filename, s):
    print(s)
    with open(filename, "a") as file:
        file.write("\n" + s)
##########################